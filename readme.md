# 🌳 Giver
## Description
Giver is a website prototype that facilitates communal fundraising for organizations' objectives by providing a platform where users can interact with sponsored content, accrue a digital currency for doing so, and donate it to an organization's objective of their choosing.
